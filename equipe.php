<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-equipe">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>equipe</span><br>
                    <h1>
                        conheça o<br>
                        <span>Nosso corpo clínico</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Equipe / Corpo clínico
                    </p>
                </div>
            </div>
        </div>
    </header>

    <section id="team-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-left">
                    <h3 class="title"><span>conheça o</span><br> time de médicos</h3>
                </div>
                <div class="col-lg-6">
                    <a class="btn-default float-right" href="#.">Área multidiciplinar <i class="flaticon-right-arrow"></i></a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="owl-carousel owl-theme owl-team">
                        <div class="item">
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="equipe-detalhe.php"><i class="fas fa-plus"></i></a>
                            </div>
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="equipe-detalhe.php"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>