<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-midia">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>comunicação</span><br>
                    <h1>
                        cebrom na<br>
                        <span>midia</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Comunicação / Cebrom na midia
                    </p>
                </div>
            </div>
        </div>
    </header>

    <section id="midia">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <p class="description">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                    </p>
                </div>
            </div>

            <div class="row mt-5 mb-5">
                <?php for ($i=0; $i < 8; $i++) { ?>
                    <div class="col-lg-3">
                        <div class="item-midia" onmouseover="hoverimg('#over<?php echo $i ?>', 'display:block')" onmouseleave="hoverimg('#over<?php echo $i ?>', 'display:none')">
                            <a class="imghover" href="#.">
                                <img src="/assets/images/midia/1.png" class="img-fluid " alt="">
                            </a>
                            <div id="over<?php echo $i ?>" class="overlay">
                                <div class="desc">
                                    <p>
                                        <span>Lorem ipsum dolor</span><br>
                                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusansi architecto beatae dicta sunt explicabo.
                                    </p>
                                    <a href="#." class="float-right"><i class="fas fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            
            <div class="row mt-2">
                <div class="col-lg-12">
                    <nav aria-label="Page navigation">
                        <ul class="pagination float-right">
                            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-chevron-left"></i></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>