<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-especialidades">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>especialidades</span><br>
                    <h1>
                        consultas<br>
                        <span>clínicas</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Equipe / consultas clínicas
                    </p>
                </div>
            </div>
        </div>
    </header>
    
    <section id="especialidades-page">
        <div class="container -aux">
            <div class="alinha-acc">
                <img class="mtn remove-mobile" src="/assets/images/men-especialities.png" alt="">
                <div class="acc">
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                especialidade 1
                                </button>
                            </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="owl-carousel owl-theme owl-team">
                                                <div class="item">
                                                    <div class="box">
                                                        <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                                        <div class="text">
                                                            <h4>Matheus Fortuna</h4>
                                                            <p>Oncologista</p>
                                                        </div>
                                                        <a class="more-team" href="equipe-detalhe.php"><i class="fas fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <h3 class="title">
                                                médicos responsáveis
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                especialidade 2
                                </button>
                            </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="owl-carousel owl-theme owl-team">
                                            <div class="item">
                                                <div class="box">
                                                    <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                                    <div class="text">
                                                        <h4>Matheus Fortuna</h4>
                                                        <p>Oncologista</p>
                                                    </div>
                                                    <a class="more-team" href="equipe-detalhe.php"><i class="fas fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <h3 class="title">
                                            médicos responsáveis
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                especialidade 3
                                </button>
                            </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="owl-carousel owl-theme owl-team">
                                            <div class="item">
                                                <div class="box">
                                                    <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                                    <div class="text">
                                                        <h4>Matheus Fortuna</h4>
                                                        <p>Oncologista</p>
                                                    </div>
                                                    <a class="more-team" href="equipe-detalhe.php"><i class="fas fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <h3 class="title">
                                            médicos responsáveis
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>