<section id="newsletter">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h1 class="title"><span>Quer receber dicas e informações?</span><br>cadastre-se em<br>nossa newsletter</h1>
                </div>

                <div class="col-lg-6 align-self-center">
                    <form action="" class="form-news" accept-charset="UTF-8" method="get">
                        <div class="input-group">
                            <input style="margin-bottom: 10px;" type="text" name="name" placeholder="Cadastrar nome" class="form-control">
                            <input type="email" name="email" placeholder="Cadastrar e-mail" class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="send mb-2 border-0"><i class="fas fa-paper-plane"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>