<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <h5>Mapa do site</h5>
                    <ul class="list-unstyled">
                        <li><a href="#.">Home</a></li>
                        <li><a href="#.">Empresa</a></li>
                        <li><a href="#.">Equipe</a></li>
                        <li><a href="#.">Especialidades</a></li>
                        <li><a href="#.">Tratamentos</a></li>
                        <li><a href="#.">Projetos</a></li>
                        <li><a href="#.">Comunicação</a></li>
                        <li><a href="#.">Contato</a></li>
                    </ul>
                    <h5>Certificado</h5>
                </div>
                <div class="col-lg-2">
                    <h5>Empresa</h5>
                    <ul class="list-unstyled">
                        <li><a href="#.">Sobre</a></li>
                        <li><a href="#.">Unidades</a></li>
                    </ul>
                    <h5>Comunicação</h5>
                    <ul class="list-unstyled">
                        <li><a href="#.">Blog</a></li>
                        <li><a href="#.">Agenda de Eventos</a></li>
                        <li><a href="#.">Imprensa</a></li>
                    </ul>
                </div>
                <div class="col-lg-2">
                    <h5>Equipe</h5>
                    <ul class="list-unstyled">
                        <li><a href="#.">Corpo Clínico</a></li>
                        <li><a href="#.">Área Multidisciplinar</a></li>
                        <li><a href="#.">Outros Departamentos</a></li>
                    </ul>
                    <h5>Contato</h5>
                    <ul class="list-unstyled">
                        <li><a href="#.">Área de RH</a></li>
                    </ul>
                </div>
                <div class="col-lg-2">
                    <h5>Especialidades</h5>
                    <ul class="list-unstyled">
                        <li><a href="#.">Consultas Clínicas</a></li>
                        <li><a href="#.">Exames Clínicos</a></li>
                    </ul>
                    <h5>Tratamentos</h5>
                    <ul class="list-unstyled">
                        <li><a href="#.">Área de RH</a></li>
                    </ul>
                </div>
                <div class="col-lg-2">
                    <h5>Projetos</h5>
                    <ul class="list-unstyled">
                        <li><a href="#.">AHPO</a></li>
                        <li><a href="#.">CEAFE</a></li>
                        <li><a href="#.">CEAMA/CEAFAM</a></li>
                        <li><a href="#.">CPOC/PEC</a></li>
                        <li><a href="#.">Cluebe de Beneficios</a></li>
                        <li><a href="#.">Parcerias</a></li>
                    </ul>
                </div>
                <div class="col-lg-2">
                    <h5>Entre em contato</h5>
                    <ul class="list-unstyled">
                        <li>
                            <p>
                                <span>UND. Universitário</span><br>
                                Fone: (62) 3265-0400
                            </p>
                            <p>
                                5° Avenida n°180, Setor Universitário, Goiânia - GO.<br>
                                Horário de Funcionamento:
                            </p>
                            <p>
                                De segunda a sexta:<br>
                                <span>07:00hs às 20:00hs</span>
                            </p>
                        </li>
                        <li>
                            <p>
                                <span>UND. Bueno</span><br>
                                Fone: (62) 3265-0400
                            </p>
                            <p>
                                5° Avenida n°180, Setor Universitário, Goiânia - GO.<br>
                                Horário de Funcionamento:
                            </p>
                            <p>
                                De segunda a sexta:<br>
                                <span>07:00hs às 20:00hs</span>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-lg-6">
                    <p class="copy">Todos os direitos reservados. 2019 | CEBROM</p>
                </div>
                <div class="col-lg-6">
                    <p class="dev">Desenvolvido por <a href="http://creativestudiodesigner.com.br" target="_BLANK">Creative Studio Design</a></p>
                </div>
            </div>
        </div>
    </footer>