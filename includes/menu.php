        <div>
            <div class="menu-wrap">
                <nav class="menu">
                    <div class="icon-list">
                        <a href="#header"><span>Home</span></a>
                        <a href="empresa.php"><span>Empresa</span></a>
                        <a href="#produtos"><span>Equipe</span></a>
                        <a href="#quem-somos"><span>Tratamentos</span></a>
                        <a href="#clientes"><span>Projetos</span></a>
                        <a href="#clientes"><span>Comunicação</span></a>
                        <a href="#contato"><span>Contato</span></a>
                    </div>
                </nav>
                <button class="close-button" id="close-button">Close Menu</button>
            </div>
            <button class="menu-button " id="open-button"></button>
        </div>

        <nav class="navbar navbar-expand-lg justify-content-end navbar-light nav-color">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/logo.png" style="max-width: 175px" alt="Cebrom" title="TipBrasil"></a>
                

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#header" title="Home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="empresa.php" title="Empresa">Empresa</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#produtos" title="Equipe">Equipe</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#quem-somos" title="Tratamentos">Tratamentos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#clientes" title="Projetos">Projetos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contato" title="Comunicação">Comunicação</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" target="_BLANK" title="Contato">Contato</a>
                        </li>
                        <li class="nav-item">
                            <div class="inputgroupicon">
                                <input type="search" name="" class="input-search" placeholder="Pesquisar" id="">
                                <i class="fas fa-search icon-search"></i>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>