<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-contato">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>contato</span><br>
                    <h1>
                        fale conosco
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Contato
                    </p>
                </div>
            </div>
        </div>
    </header>

    <section id="contato">
        <div class="container mb-3">
            <div class="row">
                <div class="col-lg-5">
                    <h3>Fale conosco</h3>
                    <p>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <?php include 'includes/form.php' ?>
                </div>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>