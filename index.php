<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-home">
        
        <?php include('includes/menu.php') ?>

        <div class="content-carousel">
            <div class="owl-carousel">
                <div>
                    <div class="col-lg-6 offset-lg-5">
                        <img alt="assets/images/slide/thumb/dot-01.png" src="assets/images/slide/slide-1.png">
                    </div>
                    <div class="col-lg-4">
                        <div class="description">
                            <h3 class="title"><span class="title-one">nós cuidamos</span><br>de você <br>com total<br><span class="title-gradient">segurança</span></h3>
                            <p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, iure eum corporis commodi placeat sit officia dicta illo quae nulla, dolores reprehenderit id eligendi quod est aut voluptas, molestias molestiae?</p>
                            <a class="btn-transparent" href="/noticias.php">Acessar noticia <i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="col-lg-6 offset-lg-5">
                        <img alt="assets/images/slide/thumb/dot-01.png" src="assets/images/slide/slide-1.png">
                    </div>
                    <div class="col-lg-4">
                        <div class="description">
                            <h3 class="title"><span class="title-one">nós cuidamos</span><br>de você <br>com total<br><span class="title-gradient">segurança</span></h3>
                            <p class="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, iure eum corporis commodi placeat sit officia dicta illo quae nulla, dolores reprehenderit id eligendi quod est aut voluptas, molestias molestiae?</p>
                            <a class="btn-transparent" href="/noticias.php">Acessar noticia <i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="especialties">
        <div class="especiality esp1">
            <div class="container">
                <i class="icon fas fa-heartbeat"></i>
                <h2>
                    <span>consultas</span><br>
                    clínicas
                </h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, cumque perspiciatis harum quos adipisci rerum sequi distinctio atque minima sunt! Dolore perferendis nesciunt beatae cumque velit iusto id quo fugit.</p>
                <a class="btn-transparent float-right" href="/noticias.php">Acesse <i class="flaticon-right-arrow"></i></a>
            </div>
        </div>
        <div class="especiality esp2">
            <div class="container">
                <i class="icon fas fa-file-medical-alt"></i>
                <h2>
                    <span>exames</span><br>
                    clínicos
                </h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum, cumque perspiciatis harum quos adipisci rerum sequi distinctio atque minima sunt! Dolore perferendis nesciunt beatae cumque velit iusto id quo fugit.</p>
                <a class="btn-transparent float-right" href="/noticias.php">Acesse <i class="flaticon-right-arrow"></i></a>
            </div>
        </div>
    </section>

    <section id="about" style="min-height: 814px;">
        <div class="container paux">
            <div class="row">
                <div class="col-lg-10 offset-lg-2">
                    <div class="row">
                        <div class="col-lg-5 offset-lg-2">
                            <h2>
                                <span>conheça</span><br>
                                    nossa história
                            </h2>
                            <p>
                                <strong>Lorem ipsum dolor sit amet consectetur adipisicing elit. </strong>
                                Laborum, cumque perspiciatis harum quos adipisci rerum sequi distinctio atque minima sunt! Dolore perferendis nesciunt beatae cumque velit iusto id quo fugit.
                            </p>
                            <a class="btn-default float-left" href="/noticias.php">Ver toda jornada <i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-10 offset-lg-2">
                            <div class="align-owl">
                        <div class="owl-carousel owl-theme owl-about">
                            <div class="item item-boxline">
                                <div class="timebox">
                                    <i class="fas fa-pills"></i><br>
                                    <strong>2012</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis optio tempora suscipit explicabo odit, ratione quibusdam.</p>
                                </div>
                            </div>
                            <div class="item item-boxline">
                                <div class="timebox">
                                    <i class="fas fa-pills"></i><br>
                                    <strong>2012</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis optio tempora suscipit explicabo odit, ratione quibusdam.</p>
                                </div>
                            </div>
                            <div class="item item-boxline">
                                <div class="timebox">
                                    <i class="fas fa-pills"></i><br>
                                    <strong>2012</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis optio tempora suscipit explicabo odit, ratione quibusdam.</p>
                                </div>
                            </div>
                            <div class="item item-boxline">
                                <div class="timebox">
                                    <i class="fas fa-pills"></i><br>
                                    <strong>2012</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis optio tempora suscipit explicabo odit, ratione quibusdam.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="units">
        <div class="title-float">
            <h1>nossas unidades</h1>
        </div>
        <div class="container -aux">
            <div class="row">
                <div class="col-lg-9" style="z-index: -1;">
                    <div class="owl-carousel owl-theme owl-units">
                        <div class="item">
                            <img src="/assets/images/units/1.png" alt="">
                        </div>
                        <div class="item">
                            <img src="/assets/images/units/1.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="title">
                        <span>unidade</span>
                        <h2 class="align-h2">univer</h2><h2>sitário</h2>
                        <i class="color-default fas fa-map-marker-alt"></i> Avenida Paulista, 355 - São Paulo<br>
                        <i class="color-default fas fa-envelope"></i> email@email.com<br>
                        <i class="color-default fas fa-phone"></i> (11) 12345 6789<br>
                        <a class="btn-default mt-3" href="#.">Entrar em contato <i class="flaticon-right-arrow"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tratamentos">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="title"><span>Nossos</span><br> Tratamentos</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <span class="title-float">01</span><br>
                    <h3 class="subtitle">quimioterapia</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit expedita placeat sed magnam, ullam unde dolorum praesentium ducimus cupiditate molestias, suscipit laudantium perferendis eum voluptas quae? Modi quo fugiat vel!</p>
                    <a class="btn-link" href="#.">Continue lendo <i class="flaticon-right-arrow"></i></a>
                </div>
                <div class="col-lg-4">
                    <span class="title-float">02</span><br>
                    <h3 class="subtitle">rádioterapia</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit expedita placeat sed magnam, ullam unde dolorum praesentium ducimus cupiditate molestias, suscipit laudantium perferendis eum voluptas quae? Modi quo fugiat vel!</p>
                    <a class="btn-link" href="#.">Continue lendo <i class="flaticon-right-arrow"></i></a>
                </div>
                <div class="col-lg-4">
                    <span class="title-float">03</span><br>
                    <h3 class="subtitle">braquiterapia</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit expedita placeat sed magnam, ullam unde dolorum praesentium ducimus cupiditate molestias, suscipit laudantium perferendis eum voluptas quae? Modi quo fugiat vel!</p>
                    <a class="btn-link" href="#.">Continue lendo <i class="flaticon-right-arrow"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section id="team">
        <div class="container -aux">
            <div class="row">
                <div class="col-lg-3 align-self-center">
                    <h3 class="title rotaten90 text-center"><span>conheça</span><br> nossa equipe</h3>
                </div>
                <div class="col-lg-9 paux align-self-center">
                    <div class="owl-carousel owl-theme owl-team">
                        <div class="item">
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="#."><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="#."><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="#."><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="#."><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="#."><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="#."><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt-3">
            <div class="row">
                <div class="col-lg-6 offset-lg-6">
                    <span class="cat">Categorias:</span>
                    <div class="dropdown">
                        <button class="btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Equipe de médicos
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                    <a class="btn-link link" href="#.">Ver toda equipe <i class="flaticon-right-arrow"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section id="clientes">
        <div class="row">
            <div class="col-lg-12 text-center">
                    <h3 class="title"><span>conheça</span><br> nossos projetos</h3>
                </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <div id="clients-slide" class="owl-carousel">
                    <div><img src="assets/images/clients/04.png" alt="Nossos projetos" title="Nossos projetos" /></div>
                    <div><img src="assets/images/clients/01.png" alt="Nossos projetos" title="Nossos projetos" /></div>
                    <div><img src="assets/images/clients/02.png" alt="Nossos projetos" title="Nossos projetos" /></div>
                    <div><img src="assets/images/clients/03.png" alt="Nossos projetos" title="Nossos projetos" /></div>
                    <div><img src="assets/images/clients/05.png" alt="Nossos projetos" title="Nossos projetos" /></div>
                    <div><img src="assets/images/clients/06.png" alt="Nossos projetos" title="Nossos projetos" /></div>
                    <div><img src="assets/images/clients/07.png" alt="Nossos projetos" title="Nossos projetos" /></div>
                    <div><img src="assets/images/clients/08.png" alt="Nossos projetos" title="Nossos projetos" /></div>
                </div>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>