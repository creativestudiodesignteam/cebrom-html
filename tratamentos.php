<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-tratamento">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>tratamentos</span><br>
                    <h1>
                        quimioterapia
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Tratamentos / Quimioterapia
                    </p>
                </div>
            </div>
        </div>
    </header>

    <section id="about-tratamento">
       <div class="row">
            <div class="col-lg-7 text-left bg-about">
                <div class="container paux">
                    <h3 class="title"><span>seduct</span><br>Lorem ipsum dolor</h3>
                    <p class="bold">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam. </p>
                    <p>voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatu</p>
                </div>
            </div>
            <div class="col-lg-5 p-0">
                <div class="owl-carousel owl-theme owl-unistimg">
                        <div class="item">
                            <img src="/assets/images/tratamentos/slide1.png" alt="">
                        </div>
                        <div class="item">
                            <img src="/assets/images/tratamentos/slide1.png" alt="">
                        </div>
                        <div class="item">
                            <img src="/assets/images/tratamentos/slide1.png" alt="">
                        </div>
                        <div class="item">
                            <img src="/assets/images/tratamentos/slide1.png" alt="">
                        </div>
                    </div>
            </div>
        </div>
    </section>

    <section id="faq">
        <div class="container -aux">
            <div class="alinha-acc">
                <img class="mtn remove-mobile" src="/assets/images/tratamentos/womanfaq.png" alt="">
                <div class="acc">
                    <h3 class="title">estamos com<br>você nessa luta</h3>
                    <p class="bold mb-4">Aqui você encontra as principais dúvidas em relação<br>
                        a quimioterapia fique tranquilo(a) qualquer dúvida conte conosco</p>
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Lorem ipsum dolor sit amet
                                </button>
                            </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed sint tempore esse saepe quod. Voluptatem blanditiis doloribus ex earum quo cumque ipsum quisquam quaerat nesciunt. Voluptates, voluptatum? Laudantium, suscipit et.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Lorem ipsum dolor sit amet
                                </button>
                            </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed sint tempore esse saepe quod. Voluptatem blanditiis doloribus ex earum quo cumque ipsum quisquam quaerat nesciunt. Voluptates, voluptatum? Laudantium, suscipit et.</p>
                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Lorem ipsum dolor sit amet
                                </button>
                            </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed sint tempore esse saepe quod. Voluptatem blanditiis doloribus ex earum quo cumque ipsum quisquam quaerat nesciunt. Voluptates, voluptatum? Laudantium, suscipit et.</p>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="phrasestruct">
        <h2>Conte conosco<br>conheça nossa estrutura</h2>
    </section>

    <section id="struct">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-3">
                    <img src="/assets/images/struct/1.png" class="img-fluid">
                </div>
                <div class="col-lg-4 mb-3">
                    <img src="/assets/images/struct/1.png" class="img-fluid">
                </div>
                <div class="col-lg-4 mb-3">
                    <img src="/assets/images/struct/1.png" class="img-fluid">
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-12 text-center">
                    <a href="#." class="btn-default">Entre em contato</a>
                </div>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>