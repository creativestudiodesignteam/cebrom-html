<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-unidade">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <h1>
                        unidade I<br>
                        <span>universitário</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Empresa / Unidade / Universitário
                    </p>
                </div>
            </div>
        </div>
    </header>

    <section id="about-unit">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-left">
                    <h3 class="title"><span>seduct</span><br>Lorem ipsum dolor</h3>
                    <p class="bold">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam. </p>
                    <p>voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatu</p>
                </div>
                <div class="col-lg-6">
                    <div class="owl-carousel owl-theme owl-unistimg">
                            <div class="item">
                                <img src="/assets/images/units/2.png" class="img-fluid br20" alt="">
                            </div>
                            <div class="item">
                                <img src="/assets/images/units/2.png" class="img-fluid br20" alt="">
                            </div>
                            <div class="item">
                                <img src="/assets/images/units/2.png" class="img-fluid br20" alt="">
                            </div>
                            <div class="item">
                                <img src="/assets/images/units/2.png" class="img-fluid br20" alt="">
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>

    <section id="phrase">
        <h2>mais que uma clínica, somos uma família</h2>
    </section>

    <section id="especialidades">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-left">
                    <h3 class="title">
                        <span>Aqui você encontra</span><br>
                        Exames e Atendimento<br>
                        Multidisciplinar Especializado
                    </h3>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-lg-6">
                    <div class="alinha-acc">
                        <div class="acc">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        especialidade 1
                                        </button>
                                    </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, omnis, eos vel odit autem nesciunt, eum deleniti totam cumque dolor ad laborum quaerat facilis maiores? Omnis incidunt suscipit debitis consequuntur!</p>
                                    </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        especialidade 2
                                        </button>
                                    </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, omnis, eos vel odit autem nesciunt, eum deleniti totam cumque dolor ad laborum quaerat facilis maiores? Omnis incidunt suscipit debitis consequuntur!</p>
                                    </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        especialidade 3
                                        </button>
                                    </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, omnis, eos vel odit autem nesciunt, eum deleniti totam cumque dolor ad laborum quaerat facilis maiores? Omnis incidunt suscipit debitis consequuntur!</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="alinha-acc">
                        <div class="acc">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        especialidade 1
                                        </button>
                                    </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, omnis, eos vel odit autem nesciunt, eum deleniti totam cumque dolor ad laborum quaerat facilis maiores? Omnis incidunt suscipit debitis consequuntur!</p>
                                    </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                    <h5 class="mb-0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        especialidade 2
                                        </button>
                                    </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, omnis, eos vel odit autem nesciunt, eum deleniti totam cumque dolor ad laborum quaerat facilis maiores? Omnis incidunt suscipit debitis consequuntur!</p>
                                    </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                        <button class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        especialidade 3
                                        </button>
                                    </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, omnis, eos vel odit autem nesciunt, eum deleniti totam cumque dolor ad laborum quaerat facilis maiores? Omnis incidunt suscipit debitis consequuntur!</p>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="team" class="-aux-team">
        <div class="container">
            <div class="row rotaten90">
                <div class="col-lg-12 text-center">
                    <h3 class="title"><span>conheça o</span><br> Time de médicos <br> dessa unidade</h3>
                </div>
            </div>
        </div>
        <div class="container -aux">
            <div class="row">
                <div class="col-lg-11 offset-lg-1">
                    <div class="owl-carousel owl-theme owl-team">
                        <div class="item">
                            <div class="box">
                                <img src="/assets/images/team/1.png" class="img-fluid img-float">
                                <div class="text">
                                    <h4>Matheus Fortuna</h4>
                                    <p>Oncologista</p>
                                </div>
                                <a class="more-team" href="#."><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt-3">
            <div class="row">
                <div class="col-lg-6 offset-lg-6">
                    <a class="btn-link link float-right" href="#.">Ver toda equipe <i class="flaticon-right-arrow"></i></a>
                </div>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>