<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-equipe">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>equipe</span><br>
                    <h1>
                        conheça o<br>
                        <span>Nosso corpo clínico</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Equipe / Corpo clínico
                    </p>
                </div>
            </div>
        </div>
    </header>

    <section id="team-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 text-center">
                    <div class="box">
                        <img src="/assets/images/team/1.png" class="img-fluid img-float">
                        <div class="text">
                            <h4>Matheus Fortuna</h4>
                            <p class="text-left">Oncologista</p>
                        </div>
                    </div>
                    <p class="cro">CRO - 10345030</p>
                    <a href="#." class="btn-download"><i class="far fa-file-pdf"></i> Baixar CV completo</a>
                </div>
                <div class="col-lg-9 pt-100">
                    <h3 class="title">Quem sou eu</h3>
                    <div class="row">
                        <div class="col-lg-6">
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Amet officiis magnam quibusdam voluptatibus alias voluptas omnis sint iste est quos? Asperiores id laudantium commodi repudiandae officia eius libero cupiditate voluptas?</p>
                        </div>
                        <div class="col-lg-6">
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit asperiores nulla modi neque mollitia officiis, porro doloribus optio deserunt culpa sapiente, ipsa, exercitationem veniam maiores repudiandae dolorem error necessitatibus quod.</p>
                        </div>
                        <div class="col-md-12">
                            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Impedit asperiores nulla modi neque mollitia officiis, porro doloribus optio deserunt culpa sapiente, ipsa, exercitationem veniam maiores repudiandae dolorem error necessitatibus quod.</p>
                        </div>
                    </div>
                    <h3 class="title mt-3">Área de Atuação</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                <li>area 1</li>
                                <li>area 2</li>
                                <li>area 3</li>
                            </ul>
                        </div>
                    </div>
                    <h3 class="title mt-3">Certificados</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <p>teste de certificado - 2001</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>