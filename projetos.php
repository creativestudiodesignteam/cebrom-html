<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-projeto">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>projetos</span><br>
                    <h1>
                        ceafam / ceama
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Projetos / CEAFAM - CEAMA
                    </p>
                </div>
            </div>
        </div>
    </header>

    <section id="about-project">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-left">
                    <div class="row mb-5">
                        <div class="col-lg-6">
                            <img src="/assets/images/projects/ceafam.png" class="img-fluid">
                        </div>
                        <div class="col-lg-6 pt-5">
                            <img src="/assets/images/projects/ceama.png" class="img-fluid">
                        </div>
                    </div>
                    <h3 class="title">O que é o<br>Ceama e o Ceafam?</h3>
                    <p>
                        O <span class="bold">Ceama - Centro de amigos da Mama e o Ceafam - Centro de Apoio a Família </span>
                        são programas de convivência e orientação, sem fins lucrativos, direcionados a todas as pessoas que estão em fase de diagnóstico e tratamento do câncer, amigos e familiares.
                    </p>
                    <p>
                      Os grupos iniciaram suas atividades em 2004, e estão direcionados também, para os pacientes que terminaram seu tratamento. Atualmente, pacientes que foram acometidos por outras doenças, também frequentam o grupo, pois o objetivo é promover a melhora na qualidade de vida. 
                    </p>

                    <h3 class="title mt-3">
                        O que é<br>oferecido?
                    </h3>
                    <p>
                       Por meio de uma ação conjunta da equipe multiprofissional e interdisciplinar, são oferecidos serviços de orientação que ajudarão na compreensão do tratamento e na reabilitação.1 
                    </p>
                </div>
                <div class="col-lg-6">
                    <div class="content-carousel">
                        <div class="owl-carousel">
                            <div>
                                <img alt="/assets/images/projects/1.png" src="/assets/images/projects/1.png" class="img-fluid br20" alt="">
                            </div>
                            
                            <div>
                                <img alt="/assets/images/projects/1.png" src="/assets/images/projects/1.png" class="img-fluid br20" alt="">
                            </div>
                            
                            <div>
                                <img alt="/assets/images/projects/1.png" src="/assets/images/projects/1.png" class="img-fluid br20" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="participar">
        <div class="container -aux">
            <img class="remove-mobile" src="/assets/images/familia-projeto.png" alt="">
            <div class="alinha-texto">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="title">Quem pode<br>participar?</h3>
                        <p>
                            O grupo é aberto a todas as pessoas. Pacientes, familiares e amigos podem participar das palestras informativas e atividades que permitam o compartilhamento de sentimentos e experiências.
                        </p>
                        <h3 class="title mt-3">Nosso<br>objetivo:</h3>
                        <p>
                            Orientar os pacientes e buscar o desenvolvimento de práticas e estratégias que reduzam as repercussões físicas, emocionais e sociais que podem advir do fato de ser acometido por um câncer.
                        </p>

                        <h3 class="title mt-3">
                            Colaboradores:
                        </h3>
                        <p>
                            Já participaram dos grupos renomados profissionais na área de Mastologia, Enfermagem, Nutrição, Psicologia, Fisioterapia, Acupuntura, Yoga, Advocacia, Sexologia, Cirurgia Plástica, Musicoterapia, Pedagogia, Artes Plásticas e outrosEncontros:
                        </p>
                        <p>
                           Os encontros serão realizados no auditório do CEBROM, na segunda Quarta Feira de cada mês, as 18:30 horas. A duração da reunião do grupo é em torno de 1h30. 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="phraseproject">
        <h2>Um projeto para a família</h2>
    </section>

    <section id="contatoproject">
        <div class="container mb-3">
            <div class="row">
                <div class="col-lg-5">
                    <h3>Fale conosco</h3>
                    <p>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7">
                    <?php include 'includes/form.php' ?>
                </div>
            </div>
        </div>
    </section>



    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>