<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-empresa">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>empresa</span><br>
                    <h1>
                        conheça<br>
                        <span>Nossa história</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Empresa / Nossa História
                    </p>
                </div>
            </div>
        </div>
    </header>
    <section id="about-page" style="min-height: 814px;">
        <div class="container paux">
            <div class="row">
                <div class="col-lg-10 offset-lg-2">
                    <div class="row">
                        <div class="col-lg-5 offset-lg-2">
                            <h2>
                                <span>conheça</span><br>
                                    nossa história
                            </h2>
                            <p>
                                <strong>Lorem ipsum dolor sit amet consectetur adipisicing elit. </strong>
                                Laborum, cumque perspiciatis harum quos adipisci rerum sequi distinctio atque minima sunt! Dolore perferendis nesciunt beatae cumque velit iusto id quo fugit.
                            </p>
                            <a class="btn-default float-left" href="/noticias.php">Ver toda jornada <i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-10 offset-lg-2">
                            <div class="align-owl">
                        <div class="owl-carousel owl-theme owl-about">
                            <div class="item item-boxline">
                                <div class="timebox">
                                    <i class="fas fa-pills"></i><br>
                                    <strong>2012</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis optio tempora suscipit explicabo odit, ratione quibusdam.</p>
                                </div>
                            </div>
                            <div class="item item-boxline">
                                <div class="timebox">
                                    <i class="fas fa-pills"></i><br>
                                    <strong>2012</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis optio tempora suscipit explicabo odit, ratione quibusdam.</p>
                                </div>
                            </div>
                            <div class="item item-boxline">
                                <div class="timebox">
                                    <i class="fas fa-pills"></i><br>
                                    <strong>2012</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis optio tempora suscipit explicabo odit, ratione quibusdam.</p>
                                </div>
                            </div>
                            <div class="item item-boxline">
                                <div class="timebox">
                                    <i class="fas fa-pills"></i><br>
                                    <strong>2012</strong>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis optio tempora suscipit explicabo odit, ratione quibusdam.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>