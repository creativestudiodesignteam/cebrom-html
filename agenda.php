<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-agenda">
        
        <?php include('includes/menu.php') ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 title">
                    <span>comunicação</span><br>
                    <h1>
                        agenda de<br>
                        <span>Eventos</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center breaditem">
                        <i class="fas fa-home"></i> Comunicação / Agenda de eventos
                    </p>
                </div>
            </div>
        </div>
    </header>

    <section id="agenda">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>
                        veja nossa agenda <br> de eventos e fique ligado
                    </h2>
                </div>
                <div class="col-lg-6">
                    <div class="float-right">
                        <span class="cat">Categorias:</span>
                        <div class="dropdown">
                            <button class="btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Selecione uma categoria
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-5">
                <?php for ($i=0; $i < 12; $i++) { ?>
                    <div class="col-lg-4">
                        <div class="item-agenda">
                            <div class="row -aux">
                                <div class="col-lg-3 date">
                                    <h2>
                                        03
                                        <span>mar</span>
                                    </h2>
                                </div>
                                <div class="col-lg-9 description">
                                    <h4>Lorem ipsum - Siamet vose</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore</p>
                                    <a href="#." class="link">Saiba mais</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>

    <?php include 'includes/newsletter.php'?>
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>

</html>